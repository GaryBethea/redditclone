class SubsController < ApplicationController

  before_filter :ensure_ownership, only: [:edit, :update]

  def new
    @sub = Sub.new

    render :new
  end

  def create
    @sub = Sub.new(sub_params)
    @sub.moderator_id = current_user.id

    if @sub.save
      redirect_to sub_url(@sub)
    else
      flash.now[:notice] = @sub.errors.full_messages

      render :new
    end
  end

  def show
    @sub = Sub.find(params[:id])
    @posts = @sub.posts

    render :show
  end

  def index
    @subs = Sub.all

    render :index
  end

  def edit
    @sub = Sub.find(params[:id])

    render :edit
  end

  def update
    @sub = Sub.find(params[:id])

    if @sub.update(sub_params)
      redirect_to sub_url(@sub)
    else
      flash.now[:notice] = @sub.errors.full_messages
      render :edit
    end
  end

  def ensure_ownership
    @sub = Sub.find(params[:id])
    unless current_user == @sub.moderator
      flash[:notice] = ["Don't touch that."]
      redirect_to subs_url
    end
  end

  private
  def sub_params
    params.require(:sub).permit(:title, :description)
  end
end
