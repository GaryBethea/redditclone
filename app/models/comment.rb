class Comment < ActiveRecord::Base
  validates :content, :author, :post_id, presence: true

  belongs_to :post, inverse_of: :comments
  belongs_to(
    :author,
    class_name: "User",
    foreign_key: :author_id,
    primary_key: :id,
    inverse_of: :comments
  )

  has_many(
    :child_comments,
    class_name: "Comment",
    foreign_key: :parent_id,
    primary_key: :id,
    inverse_of: :parent_comment,
    dependent: :destroy
  )

  belongs_to(
    :parent_comment,
    class_name: "Comment",
    foreign_key: :parent_id,
    primary_key: :id,
    inverse_of: :child_comments
  )
  
  has_many :votes, as: :votable

end
