module PostsHelper
  
  def show_children(comment)
    comment.child_comments.each do |child|
      <<-HTML.html_safe
        <ul>
          <li>
            <pre>#{ child.content }</pre>
            <a href="#{comment_url(child)}">Go to Comment</a>
            <h6>Posted by '#{child.author.email}'
              at <%= child.created_at %></h6>
          </li>
        <ul>
      HTML
    end
  end
  
end
