class CommentsController < ApplicationController

  def new
    @comment = Comment.new
    @post_id = params[:post_id]
    render :new
  end

  def create
    @comment = current_user.comments.new(comment_params)
    @comment.score = 0

    if @comment.save
      redirect_to post_url(@comment.post)
    else
      flash.now[:notice] = @comment.errors.full_messages
      render :new
    end
  end

  def show
    @comment = Comment.find(params[:id])
    render :show
  end

  def edit
    @comment = Comment.find(params[:id])
    render :edit
  end

  def update
    @comment = Comment.find(params[:id])

    if @comment.update(comment_params)
      redirect_to post_url(@comment.post)
    else
      flash.now[:notice] = @comment.errors.full_messages
      render :edit
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy

    redirect_to post_url(@comment.post)
  end
  
  def upvote
    @comment = Comment.find(params[:id])
    @comment.score += 1
    @comment.save
    
    redirect_to post_url(@comment.post)
  end
  
  def downvote
    @comment = Comment.find(params[:id])
    @comment.score -= 1
    @comment.save
    
    redirect_to post_url(@comment.post)
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :post_id, :parent_id)
  end
end
