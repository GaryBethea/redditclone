class AddScoreToPostsAndComments < ActiveRecord::Migration
  def change
    add_column :posts, :score, :integer
    add_column :comments, :score, :integer
  end
end
