class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def login!(user)
    session[:session_token] = user.reset_session_token!
  end

  def logout!
    current_user.reset_session_token!
    session[:session_token] = nil
  end

  def current_user
    User.find_by_session_token(session[:session_token])
  end

  def redirect_if_not_logged_in
    unless current_user.present?
      flash[:notice] = ["Must be logged in to view that!"]
      redirect_to new_session_url
    end
  end
  

  helper_method :current_user
end
