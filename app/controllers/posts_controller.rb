class PostsController < ApplicationController

  def new
    @post = Post.new
    @subs = Sub.all
    @source_sub_id = params[:sub_id]

    render :new
  end

  def create
    @post = current_user.posts.new(post_params)
    @post.score = 0

    if @post.save
      redirect_to post_url(@post)
    else
      flash.now[:notices] = @post.errors.full_messages
      @subs = Sub.all
      render :new
    end
  end

  def show
    @post = Post.find(params[:id])
    render :show
  end

  def edit
    @post = Post.find(params[:id])
    @subs = Sub.all

    render :edit
  end

  def update
    @post = Post.find(params[:id])
    if @post.update(post_params)

      redirect_to post_url(@post)
    else
      flash.now[:notice] = @post.errors.full_messages

      render :edit
    end
  end
  
  def upvote
    @post = Post.find(params[:id])
    @post.score += 1
    @post.save
    
    redirect_to sub_url(@post.subs.first)
  end
  
  def downvote
    @post = Post.find(params[:id])
    @post.score -= 1
    @post.save
    
    redirect_to sub_url(@post.subs.first)
  end

  # def destroy
  #   @post = Post.find(params[:id])
  #   @post.destroy
  #
  #   redirect_to subs_url
  # end

  private

  def post_params
    params.require(:post).permit(:title, :url, :content, sub_ids: [])
  end
end
